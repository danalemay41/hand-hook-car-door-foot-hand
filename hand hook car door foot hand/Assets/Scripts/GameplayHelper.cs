using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameplayHelper : MonoBehaviour
{
    public static GameplayHelper m_instance;
    public TMP_Text winnerText;
    public TMP_Text countText;

    public List<Rigidbody> limbs;
    public List<GameObject> bodies;

    private void Awake()
    {
        if (m_instance)
            Destroy(this);
        else
            m_instance = this;
    }
}
