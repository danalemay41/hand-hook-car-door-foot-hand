using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocalMovement : MonoBehaviour
{
    [SerializeField] List<Limb> bodyParts = new List<Limb>();
    public Limb currentLimb; //The currently selected limb
    private int limbId; //The index of the current limb
    public Rigidbody hand; //TEST HAND

    // Start is called before the first frame update
    void Start()
    {
        if (bodyParts != null)
        {
            currentLimb = bodyParts[0];
            bodyParts[0].Select();
            limbId = 0;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire2")) //Swap between limbs
        {
            currentLimb.Unselect();
            limbId++;
            if (limbId >= bodyParts.Count)
            {
                limbId = 0;
            }
            currentLimb = bodyParts[limbId];
            currentLimb.Select();
        }

        if(Input.GetButtonDown("Fire1"))
        {
            StartCoroutine("FalseSlap");
        }
    }

    IEnumerator FalseSlap()
    {
        for (int i = 0; i < 10; i++)
        {
            //Lerp it backward for a few frames
            Vector3 move = new Vector3(0, 0, hand.transform.position.z - 0.5f);
            Vector3 moveTo = hand.transform.position + move;
            hand.transform.position = Vector3.Lerp(hand.transform.position, moveTo, Time.deltaTime * 20);
            yield return new WaitForEndOfFrame();
        }
        for (int i = 0; i < 25; i++)
        {
            //Lerp it backward for a few frames
            Vector3 move = new Vector3(0, 0, hand.transform.position.z + 0.5f);
            Vector3 moveTo = hand.transform.position + move;
            hand.transform.position = Vector3.Lerp(hand.transform.position, moveTo, Time.deltaTime * 20);
            yield return new WaitForEndOfFrame();
        }
    }
}
