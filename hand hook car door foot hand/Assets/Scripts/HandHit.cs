using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandHit : MonoBehaviour
{
    public PlayerManager playerManager;
    public Transform bigParent;
    public void OnTriggerEnter(Collider other)
    {
        if (playerManager == null)
        {
            gameObject.SetActive(false);
        }
        if(other.transform != transform.parent || other.transform.parent != bigParent)
        {
            //Debug.Log(other.gameObject.name);
            playerManager.HandHitSomething(other);
            /*if (playerManager.isHand && playerManager.slapActive)
            {
                playerManager.HandHitSomething(other);
            } */
        } 
        
        
    }

}
