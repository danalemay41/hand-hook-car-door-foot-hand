using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour
{
    public Transform head;
    public float speed;
    private Quaternion thisRotation;
    private float xPos;
    private float yPos;
    // Start is called before the first frame update
    void Start()
    {
        thisRotation = transform.rotation;
        xPos = transform.position.x;
        yPos = transform.position.y;
    }

    // Update is called once per frame
    void Update()
    {
        transform.rotation = thisRotation;
        transform.position = new Vector3(head.position.x, yPos, head.position.z - 1.66f);
    }
}
