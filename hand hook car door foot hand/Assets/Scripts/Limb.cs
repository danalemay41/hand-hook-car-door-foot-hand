using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Limb : MonoBehaviour
{
    public Transform anchor; //What object is the limb anchored to, most likey the body
    public float maxRange; //How far from the anchor it can move
    public bool selected = false; //Is it currently selected by the player
    public float speed;
    public LocalMovement localPlayer;
    public bool isFoot; //Is this limb a foot
    public Transform body;
    private Color regColor;
    private Rigidbody rb;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        if (selected)
        {
            /*Vector3 movement = new Vector3(Input.GetAxis("Horizontal") * speed * Time.deltaTime, 0, Input.GetAxis("Vertical") * speed * Time.deltaTime);
            if (Vector3.Distance((transform.position + movement), anchor.position) < maxRange)
            {
                transform.position += movement;
                if (isFoot)
                {
                    //Move body
                    float newX = transform.position.x + (anchor.position.x - transform.position.x) / 2;
                    float newZ = transform.position.z + (anchor.position.z - transform.position.z) / 2;
                    body.position = new Vector3(newX, body.position.y, newZ);
                }
            }

            if (!isFoot && Input.GetButtonDown("Fire1"))
            {
                //Get angle between body and hand and throw something in that direction
                Vector3 dir = body.position - transform.position;
                float angle = Mathf.Atan2(dir.x, dir.z) * Mathf.Rad2Deg;
                Debug.DrawRay(transform.position, transform.position * Mathf.Abs(angle), Color.red, 10);
            } */
            Vector3 move = new Vector3(Input.GetAxis("Horizontal") * speed, 0, Input.GetAxis("Vertical") * speed);
            rb.AddForce(move);
        }
    }

    public void Select() //Change the color of the selected limb 
    {
        regColor = gameObject.GetComponent<Renderer>().material.color;
        gameObject.GetComponent<Renderer>().material.color = Color.red;
        selected = true;
    }

    public void Unselect()
    {
        selected = false;
        gameObject.GetComponent<Renderer>().material.color = regColor;
    }
}
