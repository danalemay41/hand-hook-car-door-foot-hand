using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using Photon.Pun;

public class PlayerManager : MonoBehaviourPunCallbacks
{
    public float speed = 0.1f;
    public float rotSpeed = 0.1f;

    public float handSpeed;
    public float footSpeed;
    public Rigidbody currentLimb;
    private float currentSpeed;

    public static bool hasObject = false;
    public int index;
    private bool canMove = true;
    public bool disableMovement = false;
    private bool isSlow = false;
    public bool isHand = false;
    public bool slapActive = false;
    private string ourTeam;
    public HandHit punchOb;
    public Camera playerCam;

    public void Awake()
    {
        // #Important
        // used in GameManager.cs: we keep track of the localPlayer instance to prevent instanciation when levels are synchronized
        if (photonView.IsMine)
        {
            hasObject = true;
        }

        if (!GameplayHelper.m_instance)
        {
            Debug.LogError("[PlayerManager] there is no GameplayHelper in scene.");
            return;
        }

        index = (PhotonManager.m_instance.playerNumber -1) * 2;

        currentLimb = GameplayHelper.m_instance.limbs[index];
        currentSpeed = footSpeed;
        ourTeam = currentLimb.GetComponentInParent<Ragdoll>().team;
        if(GameplayHelper.m_instance.limbs[index + 1].GetComponentInChildren<HandHit>())
        {
            punchOb = GameplayHelper.m_instance.limbs[index + 1].GetComponentInChildren<HandHit>();
            punchOb.playerManager = this;
            punchOb.gameObject.SetActive(false);
        }

        playerCam = currentLimb.GetComponentInParent<Ragdoll>().cam;
        Camera[] camList = GameObject.FindObjectsOfType<Camera>();
        for(int i = 0; i < camList.Length; i++)
        {
            if (camList[i] != playerCam)
                camList[i].GetComponent<AudioListener>().enabled = false;
        }

        photonView.RPC("ChangeLimbColor", RpcTarget.AllBuffered, GameplayHelper.m_instance.limbs.IndexOf(currentLimb), PhotonManager.m_instance.playerNumber, false);
    }

    [PunRPC]
    void UpdatePos(Vector3 _move, int limpIndex)
    {
        //GameplayHelper.m_instance.limbs[limpIndex].AddForce(_move);
        GameplayHelper.m_instance.limbs[limpIndex].transform.position = Vector3.Lerp(GameplayHelper.m_instance.limbs[limpIndex].transform.position, _move, Time.deltaTime * currentSpeed);
    }

    public override void OnDisable()
    {
        // Always call the base to remove callbacks
        base.OnDisable();
    }

    public void Update()
    {
        // we only process Inputs and check health if we are the local player
        if (photonView.IsMine && !disableMovement)
        {
            ProcessInputs();
        }
    }

    void ProcessInputs()
    {
        //Vector3 move = new Vector3(Input.GetAxis("Horizontal") * currentSpeed, 0, Input.GetAxis("Vertical") * currentSpeed);

        if(Input.GetButtonDown("Horizontal") || Input.GetButtonDown("Vertical"))
        {
            StartCoroutine("Hold");
        } 
        Vector3 move2 = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));

        Vector3 move = currentLimb.transform.position + move2;
        if(canMove)
        {
            if (!PhotonNetwork.IsMasterClient)
                photonView.RPC("UpdatePos", RpcTarget.AllBuffered, move, GameplayHelper.m_instance.limbs.IndexOf(currentLimb));
            else
                //currentLimb.AddForce(move);
                currentLimb.transform.position = Vector3.Lerp(currentLimb.transform.position, move, Time.deltaTime * currentSpeed);
        }

        if (Input.GetButtonDown("Horizontal") || Input.GetButtonDown("Vertical"))
        {
            canMove = true;
        }

        if (Input.GetButtonUp("Fire2"))
        {
            ChangeLimb();
        }

        if(Input.GetButtonDown("Fire1") && isHand && !slapActive)
        {
            StartCoroutine("StartSlap");
        }
    }

    IEnumerator Hold()
    {
        yield return new WaitForSeconds(0.3f);
        canMove = false;
    }

    IEnumerator Slapped()
    {
        isSlow = true;
        currentSpeed /= 2;
        yield return new WaitForSeconds(1);
        isSlow = false;
        currentSpeed *= 2;
    }

    IEnumerator StartSlap()
    {
        slapActive = true;
        punchOb.gameObject.SetActive(true);
        for (int i = 0; i < 10; i++)
        {
            //Lerp it backward for a few frames
            Vector3 move = new Vector3(currentLimb.transform.position.x, currentLimb.transform.position.y, currentLimb.transform.position.z - 0.5f);
            Vector3 moveTo = currentLimb.transform.position + move;
            if (!PhotonNetwork.IsMasterClient)
                photonView.RPC("UpdatePos", RpcTarget.AllBuffered, move, GameplayHelper.m_instance.limbs.IndexOf(currentLimb));
            else
                currentLimb.transform.position = Vector3.Lerp(currentLimb.transform.position, moveTo, Time.deltaTime * currentSpeed);
            yield return new WaitForSeconds(0.01f);
        }
        for (int i = 0; i < 20; i++)
        {
            //Lerp it forward for a few frames
            Vector3 move = new Vector3(currentLimb.transform.position.x, currentLimb.transform.position.y, currentLimb.transform.position.z + 0.5f);
            Vector3 moveTo = currentLimb.transform.position + move;
            if (!PhotonNetwork.IsMasterClient)
                photonView.RPC("UpdatePos", RpcTarget.AllBuffered, move, GameplayHelper.m_instance.limbs.IndexOf(currentLimb));
            else
                currentLimb.transform.position = Vector3.Lerp(currentLimb.transform.position, moveTo, Time.deltaTime * currentSpeed);
            yield return new WaitForSeconds(0.01f);
        }
        punchOb.gameObject.SetActive(false);
        canMove = true;
        slapActive = false;
    }

    public void HandHitSomething(Collider other)
    {
        if (isHand)
        {
            if (other.gameObject.tag == "Player" && other.GetComponentInParent<Ragdoll>().team != ourTeam)
            {
                photonView.RPC("GetSlapped", RpcTarget.AllBuffered, other.GetComponentInParent<Ragdoll>().team);
            }
        }
    }

    public void ChangeLimb()
    {
        Rigidbody temp = currentLimb;
        if (currentLimb == GameplayHelper.m_instance.limbs[index+1]) //Switch from hand to foot
        {
            currentLimb = GameplayHelper.m_instance.limbs[index];
            if(isSlow)
                currentSpeed = footSpeed / 2;
            else
                currentSpeed = footSpeed;
            isHand = false;
        }
        else
        {
            currentLimb = GameplayHelper.m_instance.limbs[index+1];
            if (isSlow)
                currentSpeed = handSpeed / 2;
            else
                currentSpeed = handSpeed;
            isHand = true;
        }

        photonView.RPC("ChangeLimbColor", RpcTarget.AllBuffered, GameplayHelper.m_instance.limbs.IndexOf(temp), PhotonManager.m_instance.playerNumber, true);
        photonView.RPC("ChangeLimbColor", RpcTarget.AllBuffered, GameplayHelper.m_instance.limbs.IndexOf(currentLimb), PhotonManager.m_instance.playerNumber, false);
    }  
    
    [PunRPC]
    void ChangeLimbColor(int limbIndex, int playerNumber, bool defaultLimb)
    {
        if(defaultLimb)
            GameplayHelper.m_instance.limbs[limbIndex].GetComponent<Renderer>().material = Resources.Load<Material>("DefaultLimb");
        else
            GameplayHelper.m_instance.limbs[limbIndex].GetComponent<Renderer>().material = Resources.Load<Material>("Player" + playerNumber);
    }

    [PunRPC]
    void SetDisableMovement(bool set)
    {
        disableMovement = set;
    }

    [PunRPC]
    void UpdateCountDown(string s)
    {
        GameplayHelper.m_instance.countText.text = s;
    }

    [PunRPC]
    void DisableCountDown()
    {
        GameplayHelper.m_instance.countText.gameObject.SetActive(false);
    }

    [PunRPC]
    void GetSlapped(string teamName)
    {
        if(ourTeam == teamName && !isSlow)
        {
            Debug.Log("Get slapped you idiot");
            StartCoroutine("Slapped");
        }
    }
}
