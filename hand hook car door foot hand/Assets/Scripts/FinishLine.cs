using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Realtime;
using ExitGames.Client.Photon;
using Photon.Pun;

public class FinishLine : MonoBehaviour
{
    RaycastHit hit;
    bool hasHit = false;

    void Update()
    {
        if (Physics.BoxCast(transform.position, transform.localScale/2, transform.forward, out hit))
        {
            Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * hit.distance, Color.yellow);
            if (hit.transform.tag == "Player")
            {
                Debug.Log("hit");
                if (PhotonNetwork.IsMasterClient && !hasHit)
                {
                    hasHit = true;
                    GameManager.m_instance.Finish(hit.transform.GetComponentInParent<Ragdoll>().team);
                }
            }
        }
        else
        {
            Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * 1000, Color.white);
        }
    }
}
