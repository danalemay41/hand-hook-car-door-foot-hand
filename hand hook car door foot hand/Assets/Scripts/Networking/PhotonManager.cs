using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

using Photon.Realtime;
using ExitGames.Client.Photon;
using Photon.Pun;

public class PhotonManager : MonoBehaviourPunCallbacks
{
    public static PhotonManager m_instance;

    public GameType gameType;
    public enum GameType
    {
        Play,
        Create,
        Join
    };

    public List<RoomInfo> cachedRoomList = new List<RoomInfo>();
    public List<Player> players = new List<Player>();
    public List<GameObject> playerObjects = new List<GameObject>();

    public static byte maxPlayersPerRoom = 4;
    public static string gameVersion = "1";
    string joinID;
    public int playerNumber;

    void Awake()
    {
        if (m_instance)
            Destroy(this); //destroys script
        else
        {
            m_instance = this;
            DontDestroyOnLoad(this);
        }
    }

    public void SetJoinID(string _id)
    {
        Debug.Log("[PhotonManager][SetJoinID]");
        joinID = _id;
    }

    public string GetJoinID()
    {
        Debug.Log("[PhotonManager][GetJoinID]");
        return joinID;
    }

    public override void OnDisconnected(DisconnectCause cause)
    {
        Debug.Log("[PhotonManager][OnDisconnected]");
        cachedRoomList.Clear();
        players.Clear();
    }

    public override void OnRoomListUpdate(List<RoomInfo> roomList)
    {
        Debug.Log("[PhotonManager][OnRoomListUpdate]");
        cachedRoomList = roomList;
    }

    public override void OnJoinedRoom()
    {
        Debug.Log("[PhotonManager][OnJoinedRoom]");
        int i = 0;
        foreach (var p in PhotonNetwork.PlayerList)
        {
            players.Add(p);
            i++;
        }
        playerNumber = i;
    }

    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        Debug.Log("[PhotonManager][OnPlayerLeftRoom]");
        players.Remove(otherPlayer);

        int i = 0;
        foreach (var p in PhotonNetwork.PlayerList)
        {
            i++;
        }
        playerNumber = i;
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        Debug.Log("[PhotonManager][OnPlayerEnteredRoom]");
        players.Add(newPlayer);
    }

    public override void OnLeftRoom()
    {
        Debug.Log("[PhotonManager][OnLeftRoom]");
        SceneManager.LoadScene("Main Menu");
        players.Clear();
    }
}
