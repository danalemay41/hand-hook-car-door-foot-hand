using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

using Photon.Realtime;
using ExitGames.Client.Photon;
using Photon.Pun;

public class LobbyManager : MonoBehaviour
{
    public List<TMP_Text> userNameText = new List<TMP_Text>();
    public Text roomID;
    public int playersReady;

    bool isLoading = false;
    public GameObject lobbyMenu;
    public GameObject loaderMenu;

    private void Start()
    {
        roomID.text = "Game ID: "+ PhotonNetwork.CurrentRoom.Name;
    }

    void Update()
    {
        if (PhotonNetwork.PlayerList.Length == PhotonManager.maxPlayersPerRoom && PhotonNetwork.IsMasterClient && !isLoading && playersReady > 4)
        {
            lobbyMenu.SetActive(false);
            loaderMenu.SetActive(true);
            StartCoroutine(WaitToLoad());
            isLoading = true;
        }

        for(int i = 0; i < userNameText.Count; i++)
        {
            if (PhotonManager.m_instance.players.Count > i)
            {
                userNameText[i].text = PhotonManager.m_instance.players[i].NickName;
            }
            else
            {
                userNameText[i].text = "<i>Searching...</i>";
            }
        }
    }

    //this will be removed, needed for debugging
    public void PlayGameNow()
    {
        lobbyMenu.SetActive(false);
        loaderMenu.SetActive(true);
        StartCoroutine(WaitToLoad());
        isLoading = true;
    }

    public void Leave()
    {
        PhotonNetwork.Disconnect();
        SceneManager.LoadScene("Main Menu");
    }

    public void ReadyUp()
    {
        gameObject.GetPhotonView().RPC("SetReadyUp", RpcTarget.AllBuffered);
    }

    [PunRPC]
    void SetReadyUp()
    {
        playersReady++;
    }

    IEnumerator WaitToLoad()
    {
        yield return new WaitForSeconds(3f);
        if (PhotonNetwork.IsMasterClient)
            PhotonNetwork.LoadLevel("Gameplay");
    }
}
