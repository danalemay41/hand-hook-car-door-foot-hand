using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Photon.Pun;
using Photon.Realtime;

public class GameManager : MonoBehaviourPunCallbacks
{
    static public GameManager m_instance;
    private GameObject m_objectInstance;

    public GameObject playerPrefab;

    void Start()
    {
        if (m_instance)
            Destroy(this);
        else
            m_instance = this;

        // in case we started this demo with the wrong scene being active, simply load the menu scene
        if (!PhotonNetwork.IsConnected)
        {
            Debug.Log("[GameManager] PhotonNetwork.IsConnected");
            SceneManager.LoadScene("Gameplay");

            return;
        }

        if (playerPrefab == null)
        { // #Tip Never assume public properties of Components are filled up properly, always check and inform the developer of it.

            Debug.LogError("<Color=Red><b>Missing</b></Color> playerPrefab Reference. Please set it up in GameObject 'Game Manager'", this);
        }
        else
        {
            if (!PlayerManager.hasObject)
            {
                Debug.LogFormat("[GameManager] Instantiating LocalPlayer");
                m_objectInstance = PhotonNetwork.Instantiate(this.playerPrefab.name, new Vector3(0f, 2.5f, 0f), Quaternion.identity);
                PhotonManager.m_instance.playerObjects.Add(m_objectInstance);
            }
            else
            {
                Debug.LogFormat("[GameManager] Ignoring scene load for {0}", SceneManagerHelper.ActiveSceneName);
            }
        }

        if (PhotonNetwork.IsMasterClient)
            StartCoroutine(StartGameTimer());
    }   

    void LoadArena()
    {
        Debug.Log("[GameManager][LoadArena]");
        if (!PhotonNetwork.IsMasterClient)
        {
            Debug.LogError("[GameManager] PhotonNetwork : Trying to Load a level but we are not the master Client");
        }

        Debug.LogFormat("[GameManager] PhotonNetwork : Loading Level : {0}", PhotonNetwork.CurrentRoom.PlayerCount);

        PhotonNetwork.LoadLevel("Gameplay");
    }

    public void Finish(string teamName)
    {
        Debug.Log("Team " + teamName + " won!");
        GameplayHelper.m_instance.winnerText.gameObject.SetActive(true);

        if (teamName == "1")
        {
            GameplayHelper.m_instance.winnerText.text = "Team 1 Wins!";
        }
        else if(teamName == "2")
        {
            GameplayHelper.m_instance.winnerText.text = "Team 2 Wins!";
        }
        m_objectInstance.GetPhotonView().RPC("SetDisableMovement", RpcTarget.AllBuffered, true);

        if (PhotonNetwork.IsMasterClient)
            StartCoroutine(EndGameTimer());
    }

    IEnumerator StartGameTimer()
    {
        yield return new WaitForSeconds(0.5f);
        m_objectInstance.GetPhotonView().RPC("SetDisableMovement", RpcTarget.AllBuffered, true);
        float totalTime = 3;

        while (totalTime > 0.6f)
        {
            totalTime -= Time.deltaTime;
            //GameplayHelper.m_instance.countText.text = Mathf.Round(totalTime).ToString();
            m_objectInstance.GetPhotonView().RPC("UpdateCountDown", RpcTarget.AllBuffered, Mathf.Round(totalTime).ToString());
            yield return null;
        }

        //GameplayHelper.m_instance.countText.text = "GO!";
        m_objectInstance.GetPhotonView().RPC("UpdateCountDown", RpcTarget.AllBuffered, "GO!");
        yield return new WaitForSeconds(1f);
        //GameplayHelper.m_instance.countText.gameObject.SetActive(false);
        m_objectInstance.GetPhotonView().RPC("DisableCountDown", RpcTarget.AllBuffered);
        m_objectInstance.GetPhotonView().RPC("SetDisableMovement", RpcTarget.AllBuffered, false);
    }

    IEnumerator EndGameTimer()
    {
        yield return new WaitForSeconds(5f);
        if (PhotonNetwork.IsMasterClient)
            PhotonNetwork.LoadLevel("Lobby");
    }
}
