using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

using Photon.Realtime;
using ExitGames.Client.Photon;
using Photon.Pun;

public class LaunchGame : MonoBehaviourPunCallbacks
{
    public Text connectionFeedback;
    public GameObject loaderAnime;

    bool isConnecting = false;

    private void Start()
    {
        Connect();
        if (loaderAnime != null)
        {
            loaderAnime.SetActive(true);
        }
    }

    void Awake()
    {
        PhotonNetwork.AutomaticallySyncScene = true;
    }

    /// <summary>
    /// Start the connection process.
    /// </summary>
    public void Connect()
    {
        connectionFeedback.text = "";
        isConnecting = true;

        // start the loader animation for visual effect.
        if (loaderAnime != null)
            loaderAnime.SetActive(true);

        // we check if we are connected or not, we join if we are , else we initiate the connection to the server.
        if (PhotonNetwork.IsConnected)
            ConnectByType();
        else
        {
            LogFeedback("Connecting...");

            // #Critical, we must first and foremost connect to Photon Online Server.
            PhotonNetwork.ConnectUsingSettings();
            PhotonNetwork.GameVersion = PhotonManager.gameVersion;
        }
    }

    void ConnectByType()
    {
        if (PhotonManager.m_instance.gameType == PhotonManager.GameType.Play)
        {
            LogFeedback("Joining room...");
            PhotonNetwork.JoinRandomRoom();
        }
        else if (PhotonManager.m_instance.gameType == PhotonManager.GameType.Create)
        {
            LogFeedback("Creating level...");
            PhotonNetwork.CreateRoom(RandomCode(), new RoomOptions { MaxPlayers = PhotonManager.maxPlayersPerRoom });
        }
        else if (PhotonManager.m_instance.gameType == PhotonManager.GameType.Join)
        {
            if (PhotonManager.m_instance.GetJoinID() != null)
            {
                LogFeedback("Joining room " + PhotonManager.m_instance.GetJoinID() + "...");
                PhotonNetwork.JoinRoom(PhotonManager.m_instance.GetJoinID());
            }
        }
    }

    string Rand()
    {
        int rand = Random.Range(0, 10);
        return rand.ToString();
    }

    string RandomCode()
    {
        string code = Rand() + Rand() + Rand() + Rand() + Rand();

        foreach(var room in PhotonManager.m_instance.cachedRoomList)
        {
            if (room.Name == code)
                return RandomCode();
        }
        return code;
    }

    /// <summary>
    /// Logs the feedback in the UI view for the player.
    /// </summary>
    void LogFeedback(string message)
    {
        // we do not assume there is a feedbackText defined.
        if (connectionFeedback == null)
        {
            return;
        }

        // add new messages as a new line and at the bottom of the log.
        connectionFeedback.text += System.Environment.NewLine + message;
    }

    #region Photon Callbacks
    public override void OnDisconnected(DisconnectCause cause)
    {
        LogFeedback("<Color=Red>Disconnected</Color> " + cause);
        loaderAnime.SetActive(false);
    }

    /// <summary>
    /// Called after the connection to the master is established and authenticated
    /// </summary>
    public override void OnConnectedToMaster()
    {
        if (isConnecting)
        {
            ConnectByType();
        }
    }

    /// <summary>
    /// Called when a JoinRandom() call failed.
    /// </summary>
    /// <remarks>
    /// Most likely all rooms are full or no rooms are available. <br/>
    /// </remarks>
    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        if(PhotonManager.m_instance.gameType == PhotonManager.GameType.Create)
        {
            LogFeedback("<Color=Red>Joining room failed</Color>");
            SceneManager.LoadScene("Main Menu");
        }
        else
        {
            LogFeedback("<Color=Red>Joining room failed</Color> Creating a new Room...");
            PhotonNetwork.CreateRoom(RandomCode(), new RoomOptions { MaxPlayers = PhotonManager.maxPlayersPerRoom });
        }
    }

    /// <summary>
    /// Called when entering a room (by creating or joining it). Called on all clients (including the Master Client).
    /// </summary>
    public override void OnJoinedRoom()
    {
        if (PhotonManager.m_instance.gameType == PhotonManager.GameType.Play)
            LogFeedback("Joining room...");
        else if (PhotonManager.m_instance.gameType == PhotonManager.GameType.Create)
            LogFeedback("Joining created level...");
        else if(PhotonManager.m_instance.gameType == PhotonManager.GameType.Join)
            LogFeedback("Joining level " + PhotonManager.m_instance.GetJoinID() + "...");

        if (PhotonNetwork.CurrentRoom.PlayerCount == 1)
            PhotonNetwork.LoadLevel("Lobby");
    }
    #endregion
}
