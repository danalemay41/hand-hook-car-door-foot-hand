using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;
using Photon.Realtime;
using ExitGames.Client.Photon;
using Photon.Pun;

public class MainMenuUI : MonoBehaviour
{
    public Text nameText;
    string playID;

    public void ConnectButtonPress()
    {
        //if (PhotonManager.m_instance)
        //    //PhotonManager.m_instance.Connect();
        //else
        //    Debug.LogError("[MainMenuUI] Cannot call needed function ConnectButtonPress(). PhotonManager.m_instance is null.");      
    }

    public void Play()
    {
        if (PhotonNetwork.NickName != null && nameText.text != "")
        {
            PhotonManager.m_instance.gameType = PhotonManager.GameType.Play;
            SceneManager.LoadScene("Load");
        }
    }

    public void Create()
    {
        if (PhotonNetwork.NickName != null && nameText.text != "")
        {
            PhotonManager.m_instance.gameType = PhotonManager.GameType.Create;
            SceneManager.LoadScene("Load");
        }
    }

    public void Join()
    {
        if (PhotonNetwork.NickName != null && nameText.text != "" && playID != null)
        {
            PhotonManager.m_instance.gameType = PhotonManager.GameType.Join;
            PhotonManager.m_instance.SetJoinID(playID);
            SceneManager.LoadScene("Load");
        }
    }

    public void SetPlayerName(string value)
    {
        // #Important
        if (string.IsNullOrEmpty(value))
        {
            Debug.LogError("PlayID is null or empty");
            return;
        }

        playID = value;
    }

    public void Leave()
    {
        PhotonNetwork.Disconnect();
    }

    public void QuitApplication()
    {
        Application.Quit();
    }
}
